
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <poll.h>

#define GPIO 4

/*
  TODO: The equivalent of
      echo 4       > /sys/class/gpio/export
      echo in      > /sys/class/gpio/gpio4/direction
      echo falling > /sys/class/gpio/gpio4/edge
*/

void
consume_interrupt(int fd) {
   char buf[2];

   lseek(fd, 0, SEEK_SET);
   read(fd, buf, sizeof buf);
}

int main(int argc, char *argv[]) {
   char str[32];
   struct pollfd pfd;
   int fd, gpio;

   if (argc > 1) gpio = atoi(argv[1]);
   else          gpio = GPIO;

   sprintf(str, "/sys/class/gpio/gpio%d/value", gpio);

   if ((fd = open(str, O_RDONLY)) < 0) {
      fprintf(stderr, "Failed, gpio %d not exported.\n", gpio);
      exit(1);
   }

   pfd.fd = fd;
   pfd.events = POLLPRI;

   consume_interrupt(fd); /* consume any prior interrupt */
   while (1) {
       poll(&pfd, 1, -1);         /* wait for interrupt */
       consume_interrupt(fd); 
       fprintf(stderr, "GPIO %d: boink!\n", gpio);
   }

   exit(0);
}
